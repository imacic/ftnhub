﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class UserBusiness
    {

        private UserRepository _userRepository;

        public UserBusiness()
        {
            _userRepository = new UserRepository();
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public int InsertUser(User u)
        {
            return _userRepository.InsertUser(u);
        }

        public User GetUserById(int UserId)
        {
            return _userRepository.GetAllUsers().Where(u => u.Id == UserId).FirstOrDefault();
        }
    }
}
 