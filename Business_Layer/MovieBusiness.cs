﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer
{
    public class MovieBusiness
    {
        private MovieRepository movieRepository;

        public MovieBusiness()
        {
            movieRepository = new MovieRepository();
        }

        public Movie GetMovie(int MovieId)
        {
            return movieRepository.GetAllMovies().Where(m => m.Id == MovieId).FirstOrDefault();
        }
        public List<Movie> GetAllMovies()
        {
            return movieRepository.GetAllMovies();
        }
        public List<Movie> SearchMovies(string valueToSearch)
        {
            return movieRepository.SearchMovies(valueToSearch);
        }
    }
}
