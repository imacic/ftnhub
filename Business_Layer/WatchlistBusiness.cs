﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer
{
    public class WatchlistBusiness
    {
        private WatchlistRepository watchlistRepository;

        public WatchlistBusiness()
        {
            watchlistRepository = new WatchlistRepository();
        }

        public List<Watchlist> GetAllWatchlists()
        {
            return watchlistRepository.GetAllWatchlists();
        }
        public List<Watchlist> GetWatchlist(int UserId)
        {
            return watchlistRepository.GetAllWatchlists().Where(u => u.UsersID == UserId).ToList();
        }
        public Watchlist GetWatchlistByUserMovie(int UserId, int MovieId)
        {
            return watchlistRepository.GetAllWatchlists().Where(u => u.UsersID == UserId && u.MovieID == MovieId).FirstOrDefault();
        }

        public int InsertWatchlist(Watchlist w)
        {
            return watchlistRepository.InsertWatchlist(w);
        }

        public int DeleteMovieFromWatchlist(int movieID, int userID)
        {
            return watchlistRepository.DeleteMovieFromWatchlist(movieID, userID);
        }
        public int UpdateWatchlist(int Rate, string Comment, int MovieID, int UserID)
        {
            return watchlistRepository.UpdateWatchlist(Rate, Comment, MovieID, UserID);
        }
    }
}
