﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerDesktop
{
    public partial class LoginForm : Form
    {
        public Point mouseLocation;

        UserBusiness userBusiness = new UserBusiness();
        public LoginForm()
        {
            InitializeComponent();
        }

        

        private void btnLogin_Click(object sender, EventArgs e)
        {
            List<User> lista = userBusiness.GetAllUsers();
            User newUser = new User();
            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Username == txtLogUsername.Text && lista[i].Password == txtLogPassword.Text)
                {
                    newUser.Id = lista[i].Id;
                    newUser.Username = lista[i].Username;
                    newUser.Password = lista[i].Password;
                }
            }

            if (newUser.Username != null && newUser.Password != null)
            {
                StreamWriter sw = new StreamWriter("LoggedUser.txt");
                sw.WriteLine(newUser.Id);
                sw.Close();


                this.Hide();
                WatchlistForm watchList = new WatchlistForm();
                watchList.Show();
            }

            else
                MessageBox.Show("Wrong Username and Password!");
        }


        private void txtLogUsername_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(txtLogUsername.Text))
            {
                e.Cancel = true;
                txtLogUsername.Focus();
                errorProvider.SetError(txtLogUsername, "Please enter your user name !");
            }
            else
            {
                e.Cancel = false;
                errorProvider.SetError(txtLogUsername, null);
            }
        }

        private void txtLogPassword_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(txtLogPassword.Text))
            {
                e.Cancel = true;
                txtLogPassword.Focus();
                errorProvider1.SetError(txtLogPassword, "Please enter your password !");
            }
            else
            {
                e.Cancel = false;
                errorProvider1.SetError(txtLogPassword, null);
            }
        }

        private void txtLogUsername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Use only letters and numbers!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtLogPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Use only letters and numbers!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblLogRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            RegisterForm regForm = new RegisterForm();
            regForm.Show();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseLocation = new Point(-e.X, -e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePose = Control.MousePosition;
                mousePose.Offset(mouseLocation.X, mouseLocation.Y);
                Location = mousePose;
            }
        }
    }
}
