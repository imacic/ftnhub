﻿using Business_Layer;
using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerDesktop
{
    public partial class WatchlistItemForm : Form
    {
        WatchlistBusiness watchlistBusiness = new WatchlistBusiness();
        MovieBusiness movieBusiness = new MovieBusiness();
        UserBusiness userBusiness = new UserBusiness();
        public WatchlistItemForm()
        {
            InitializeComponent();
        }

        private void lblRate_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Watchlist w = new Watchlist();

            StreamReader srM = new StreamReader("MovieItem.txt");
            int movieItemId = Int32.Parse(srM.ReadLine());
            srM.Close();

            StreamReader sr = new StreamReader("LoggedUser.txt");
            int userID = Int32.Parse(sr.ReadLine());
            sr.Close();

            w.UsersID = userID;
            w.MovieID = movieItemId;
            w.Rate = rateValue;
            w.Comment = txtComment.Text;

            watchlistBusiness.InsertWatchlist(w);

            MessageBox.Show("Succesfully added!", "Watchlist updated!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            WatchlistForm wlf = new WatchlistForm();
            wlf.listWatchlist();

            this.Close();

            

            
        }

        private void WatchlistItemForm_Load(object sender, EventArgs e)
        {
            getMovie();
        }

        private void getMovie()
        {
            StreamReader sr = new StreamReader("MovieItem.txt");
            int movieItemId = Int32.Parse(sr.ReadLine());
            sr.Close();
            Movie movie = movieBusiness.GetMovie(movieItemId);

            lblTitle.Text = movie.Title;
            itemThumbnail.ImageLocation = movie.Thumbnail;

        }

        int rateValue;
        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 1;
        }

        private void rb2_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 2;
        }

        private void rb3_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 3;
        }

        private void rb4_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 4;
        }

        private void rb5_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 5;
        }

        private void rb6_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 6;
        }

        private void rb7_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 7;
        }

        private void rb8_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 8;
        }

        private void rb9_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 9;
        }

        private void rb10_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 10;
        }

        private void btn4Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            Watchlist w = new Watchlist();

            StreamReader srM = new StreamReader("MovieItem.txt");
            int movieItemId = Int32.Parse(srM.ReadLine());
            srM.Close();

            StreamReader sr = new StreamReader("LoggedUser.txt");
            int userID = Int32.Parse(sr.ReadLine());
            sr.Close();

            w.UsersID = userID;
            w.MovieID = movieItemId;
            w.Rate = rateValue;
            w.Comment = txtComment.Text;

            List<Watchlist> allWatchlists = new List<Watchlist>();
            allWatchlists = watchlistBusiness.GetAllWatchlists();
            int counter = 0;

            foreach (Watchlist watchlist in allWatchlists)
            {
                if (w.MovieID == watchlist.MovieID && w.UsersID == watchlist.UsersID)
                {
                    counter++;
                }
            }

            if (counter == 0)
            {
                watchlistBusiness.InsertWatchlist(w);
                MessageBox.Show("Succesfully added!", "Watchlist updated!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Selected movie is already added to watchlist!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            //WatchlistForm wlf = new WatchlistForm();
            //wlf.listWatchlist();

            this.Close();
        }
    }
}
