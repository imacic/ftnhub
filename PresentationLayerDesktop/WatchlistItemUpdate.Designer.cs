﻿namespace PresentationLayerDesktop
{
    partial class WatchlistItemUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemThumbnail = new System.Windows.Forms.PictureBox();
            this.lblRate = new System.Windows.Forms.Label();
            this.lblComment = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.RichTextBox();
            this.rb1 = new System.Windows.Forms.RadioButton();
            this.rb2 = new System.Windows.Forms.RadioButton();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.rb4 = new System.Windows.Forms.RadioButton();
            this.rb5 = new System.Windows.Forms.RadioButton();
            this.rb7 = new System.Windows.Forms.RadioButton();
            this.rb8 = new System.Windows.Forms.RadioButton();
            this.rb9 = new System.Windows.Forms.RadioButton();
            this.rb10 = new System.Windows.Forms.RadioButton();
            this.rb6 = new System.Windows.Forms.RadioButton();
            this.pnlExit = new System.Windows.Forms.Panel();
            this.btn4Exit = new System.Windows.Forms.Button();
            this.lblATW = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.itemThumbnail)).BeginInit();
            this.pnlExit.SuspendLayout();
            this.SuspendLayout();
            // 
            // itemThumbnail
            // 
            this.itemThumbnail.Location = new System.Drawing.Point(21, 74);
            this.itemThumbnail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.itemThumbnail.Name = "itemThumbnail";
            this.itemThumbnail.Size = new System.Drawing.Size(172, 206);
            this.itemThumbnail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemThumbnail.TabIndex = 0;
            this.itemThumbnail.TabStop = false;
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRate.Location = new System.Drawing.Point(215, 74);
            this.lblRate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(150, 25);
            this.lblRate.TabIndex = 2;
            this.lblRate.Text = "Rate this movie:";
            this.lblRate.Click += new System.EventHandler(this.lblRate_Click);
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComment.Location = new System.Drawing.Point(215, 143);
            this.lblComment.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(182, 25);
            this.lblComment.TabIndex = 3;
            this.lblComment.Text = "Add your comment:";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(220, 171);
            this.txtComment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtComment.MaxLength = 120;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(492, 63);
            this.txtComment.TabIndex = 4;
            this.txtComment.Text = "";
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Location = new System.Drawing.Point(220, 102);
            this.rb1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(37, 21);
            this.rb1.TabIndex = 6;
            this.rb1.TabStop = true;
            this.rb1.Text = "1";
            this.rb1.UseVisualStyleBackColor = true;
            this.rb1.CheckedChanged += new System.EventHandler(this.rb1_CheckedChanged);
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.Location = new System.Drawing.Point(269, 102);
            this.rb2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(37, 21);
            this.rb2.TabIndex = 7;
            this.rb2.TabStop = true;
            this.rb2.Text = "2";
            this.rb2.UseVisualStyleBackColor = true;
            this.rb2.CheckedChanged += new System.EventHandler(this.rb2_CheckedChanged);
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Location = new System.Drawing.Point(319, 102);
            this.rb3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(37, 21);
            this.rb3.TabIndex = 8;
            this.rb3.TabStop = true;
            this.rb3.Text = "3";
            this.rb3.UseVisualStyleBackColor = true;
            this.rb3.CheckedChanged += new System.EventHandler(this.rb3_CheckedChanged);
            // 
            // rb4
            // 
            this.rb4.AutoSize = true;
            this.rb4.Location = new System.Drawing.Point(368, 102);
            this.rb4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb4.Name = "rb4";
            this.rb4.Size = new System.Drawing.Size(37, 21);
            this.rb4.TabIndex = 9;
            this.rb4.TabStop = true;
            this.rb4.Text = "4";
            this.rb4.UseVisualStyleBackColor = true;
            this.rb4.CheckedChanged += new System.EventHandler(this.rb4_CheckedChanged);
            // 
            // rb5
            // 
            this.rb5.AutoSize = true;
            this.rb5.Location = new System.Drawing.Point(417, 102);
            this.rb5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb5.Name = "rb5";
            this.rb5.Size = new System.Drawing.Size(37, 21);
            this.rb5.TabIndex = 10;
            this.rb5.TabStop = true;
            this.rb5.Text = "5";
            this.rb5.UseVisualStyleBackColor = true;
            this.rb5.CheckedChanged += new System.EventHandler(this.rb5_CheckedChanged);
            // 
            // rb7
            // 
            this.rb7.AutoSize = true;
            this.rb7.Location = new System.Drawing.Point(516, 101);
            this.rb7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb7.Name = "rb7";
            this.rb7.Size = new System.Drawing.Size(37, 21);
            this.rb7.TabIndex = 12;
            this.rb7.TabStop = true;
            this.rb7.Text = "7";
            this.rb7.UseVisualStyleBackColor = true;
            this.rb7.CheckedChanged += new System.EventHandler(this.rb7_CheckedChanged);
            // 
            // rb8
            // 
            this.rb8.AutoSize = true;
            this.rb8.Location = new System.Drawing.Point(565, 102);
            this.rb8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb8.Name = "rb8";
            this.rb8.Size = new System.Drawing.Size(37, 21);
            this.rb8.TabIndex = 13;
            this.rb8.TabStop = true;
            this.rb8.Text = "8";
            this.rb8.UseVisualStyleBackColor = true;
            this.rb8.CheckedChanged += new System.EventHandler(this.rb8_CheckedChanged);
            // 
            // rb9
            // 
            this.rb9.AutoSize = true;
            this.rb9.Location = new System.Drawing.Point(615, 102);
            this.rb9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb9.Name = "rb9";
            this.rb9.Size = new System.Drawing.Size(37, 21);
            this.rb9.TabIndex = 14;
            this.rb9.TabStop = true;
            this.rb9.Text = "9";
            this.rb9.UseVisualStyleBackColor = true;
            this.rb9.CheckedChanged += new System.EventHandler(this.rb9_CheckedChanged);
            // 
            // rb10
            // 
            this.rb10.AutoSize = true;
            this.rb10.Location = new System.Drawing.Point(664, 102);
            this.rb10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb10.Name = "rb10";
            this.rb10.Size = new System.Drawing.Size(45, 21);
            this.rb10.TabIndex = 15;
            this.rb10.TabStop = true;
            this.rb10.Text = "10";
            this.rb10.UseVisualStyleBackColor = true;
            this.rb10.CheckedChanged += new System.EventHandler(this.rb10_CheckedChanged);
            // 
            // rb6
            // 
            this.rb6.AutoSize = true;
            this.rb6.Location = new System.Drawing.Point(467, 102);
            this.rb6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rb6.Name = "rb6";
            this.rb6.Size = new System.Drawing.Size(37, 21);
            this.rb6.TabIndex = 16;
            this.rb6.TabStop = true;
            this.rb6.Text = "6";
            this.rb6.UseVisualStyleBackColor = true;
            this.rb6.CheckedChanged += new System.EventHandler(this.rb6_CheckedChanged);
            // 
            // pnlExit
            // 
            this.pnlExit.BackColor = System.Drawing.Color.Transparent;
            this.pnlExit.Controls.Add(this.btn4Exit);
            this.pnlExit.Location = new System.Drawing.Point(735, 0);
            this.pnlExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlExit.Name = "pnlExit";
            this.pnlExit.Size = new System.Drawing.Size(55, 39);
            this.pnlExit.TabIndex = 17;
            // 
            // btn4Exit
            // 
            this.btn4Exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn4Exit.FlatAppearance.BorderSize = 0;
            this.btn4Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn4Exit.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4Exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.btn4Exit.Location = new System.Drawing.Point(4, 0);
            this.btn4Exit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn4Exit.Name = "btn4Exit";
            this.btn4Exit.Size = new System.Drawing.Size(51, 39);
            this.btn4Exit.TabIndex = 0;
            this.btn4Exit.Text = "X";
            this.btn4Exit.UseVisualStyleBackColor = true;
            this.btn4Exit.Click += new System.EventHandler(this.btn4Exit_Click);
            // 
            // lblATW
            // 
            this.lblATW.AutoSize = true;
            this.lblATW.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATW.Location = new System.Drawing.Point(21, 30);
            this.lblATW.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblATW.Name = "lblATW";
            this.lblATW.Size = new System.Drawing.Size(138, 25);
            this.lblATW.TabIndex = 18;
            this.lblATW.Text = "Update movie:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Georgia", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(215, 26);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(69, 29);
            this.lblTitle.TabIndex = 19;
            this.lblTitle.Text = "Title";
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdate.ForeColor = System.Drawing.SystemColors.Control;
            this.btnUpdate.Location = new System.Drawing.Point(220, 242);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(109, 37);
            this.btnUpdate.TabIndex = 21;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // WatchlistItemUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(789, 313);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblATW);
            this.Controls.Add(this.pnlExit);
            this.Controls.Add(this.rb6);
            this.Controls.Add(this.rb10);
            this.Controls.Add(this.rb9);
            this.Controls.Add(this.rb8);
            this.Controls.Add(this.rb7);
            this.Controls.Add(this.rb5);
            this.Controls.Add(this.rb4);
            this.Controls.Add(this.rb3);
            this.Controls.Add(this.rb2);
            this.Controls.Add(this.rb1);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.lblRate);
            this.Controls.Add(this.itemThumbnail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "WatchlistItemUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "y";
            this.Load += new System.EventHandler(this.WatchlistItemUpdate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.itemThumbnail)).EndInit();
            this.pnlExit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox itemThumbnail;
        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.RichTextBox txtComment;
        private System.Windows.Forms.RadioButton rb1;
        private System.Windows.Forms.RadioButton rb2;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.RadioButton rb4;
        private System.Windows.Forms.RadioButton rb5;
        private System.Windows.Forms.RadioButton rb7;
        private System.Windows.Forms.RadioButton rb8;
        private System.Windows.Forms.RadioButton rb9;
        private System.Windows.Forms.RadioButton rb10;
        private System.Windows.Forms.RadioButton rb6;
        private System.Windows.Forms.Panel pnlExit;
        private System.Windows.Forms.Button btn4Exit;
        private System.Windows.Forms.Label lblATW;
        public System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnUpdate;
    }
}