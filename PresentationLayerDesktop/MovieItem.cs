﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DataLayer.Models;
using Business_Layer;

namespace PresentationLayerDesktop
{
    public partial class MovieItem : UserControl
    {
        MovieBusiness movieBusiness = new MovieBusiness();
        public MovieItem()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Movie> lista = movieBusiness.GetAllMovies();

            Movie movieItem = new Movie();
            movieItem.Title = lblTitle.Text;
            StreamWriter sw = new StreamWriter("MovieItem.txt");
            for (int i = 0; i < lista.Count; i++)
            {
                if (lblTitle.Text == lista[i].Title)
                {
                    movieItem.Id = lista[i].Id;
                }
            }
            sw.WriteLine(movieItem.Id);
            sw.Close();
            WatchlistItemForm watchlistItem = new WatchlistItemForm();

            watchlistItem.Show();

        }
    }
}
