﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLayer.Models;
using Business_Layer;
using System.IO;

namespace PresentationLayerDesktop
{
    public partial class ListItem : UserControl
    {
        MovieBusiness movieBusiness = new MovieBusiness();
        WatchlistBusiness watchlistBusiness = new WatchlistBusiness();
        public ListItem()
        {
            InitializeComponent();
        }

        private void ListItem_Load(object sender, EventArgs e)
        {

        }

        private void btnRmv_Click(object sender, EventArgs e)
        {
            List<Movie> lista = movieBusiness.GetAllMovies();


            string Title = lblTitle.Text;
            int movieID = 0;
            
            for (int i = 0; i < lista.Count; i++)
            {
                if (lblTitle.Text == lista[i].Title)
                {
                    movieID = lista[i].Id;
                }
            }

            StreamReader sr = new StreamReader("LoggedUser.txt");
            int userID = Int32.Parse(sr.ReadLine());
            sr.Close();

            watchlistBusiness.DeleteMovieFromWatchlist(movieID, userID);

            MessageBox.Show("Succesfully removed from Watchlist!", "Watchlist updated!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            WatchlistForm wlf = new WatchlistForm();
            wlf.listWatchlist();


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            List<Movie> lista = movieBusiness.GetAllMovies();

            Movie movieItem = new Movie();
            movieItem.Title = lblTitle.Text;
            StreamWriter sw = new StreamWriter("MovieUpdate.txt");
            for (int i = 0; i < lista.Count; i++)
            {
                if (lblTitle.Text == lista[i].Title)
                {
                    movieItem.Id = lista[i].Id;
                }
            }
            sw.WriteLine(movieItem.Id);
            sw.Close();

            WatchlistItemUpdate watchlistItemUpdate = new WatchlistItemUpdate();
            watchlistItemUpdate.Show();
        }
    }
}
