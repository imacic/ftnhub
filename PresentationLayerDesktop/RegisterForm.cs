﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;

namespace PresentationLayerDesktop
{
    public partial class RegisterForm : Form
    {
        public Point mouseLocation;

        UserBusiness userBusiness = new UserBusiness();
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            User u = new User();

            u.Username = txtUsername.Text;
            u.Password = txtPassword.Text;

            
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                MessageBox.Show("You are ready to go!", "Signed up!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                userBusiness.InsertUser(u);
                this.Hide();
                LoginForm logForm = new LoginForm();
                logForm.Show();
            }
        }

        private void txtUsername_Validating(object sender, CancelEventArgs e)
        {
            List<User> lista = userBusiness.GetAllUsers();
            User newUser = new User();
            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Username == txtUsername.Text)
                {
                    e.Cancel = true;
                    txtUsername.Focus();
                    MessageBox.Show("Username already exists!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (txtUsername.Text.Length <8)
            {
                e.Cancel = true;
                txtUsername.Focus();
                errorProvider.SetError(txtUsername, "Username must contain at least 8 characters!");
            }
            else
            {
                e.Cancel = false;
                errorProvider.SetError(txtUsername, null);
            }
            
        }

        private void txtUsername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Use only letters and numbers!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtPassword_Validating(object sender, CancelEventArgs e)
        {
            if (txtPassword.Text.Length < 8)
            {
                e.Cancel = true;
                txtPassword.Focus();
                errorProvider.SetError(txtPassword, "Password must contain at least 8 characters!");
            }
            else
            {
                e.Cancel = false;
                errorProvider.SetError(txtPassword, null);
            }
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Use only letters and numbers!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtConfirm_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(txtConfirm.Text))
            {
                e.Cancel = true;
                txtConfirm.Focus();
                errorProvider.SetError(txtConfirm, "This field must be filled.");
            }
            if (txtConfirm.Text != txtPassword.Text)
            {
                MessageBox.Show("Password and Confirm password must match!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
                errorProvider.SetError(txtConfirm, null);
            }
        }

        private void txtConfirm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Use only letters and numbers!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn2Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblLogRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void lblRegLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            LoginForm logForm = new LoginForm();
            logForm.Show();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouseLocation = new Point(-e.X, -e.Y);
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePose = Control.MousePosition;
                mousePose.Offset(mouseLocation.X, mouseLocation.Y);
                Location = mousePose;
            }
        }
    }
}
