﻿namespace PresentationLayerDesktop
{
    partial class WatchlistItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemThumbnail = new System.Windows.Forms.PictureBox();
            this.lblRate = new System.Windows.Forms.Label();
            this.lblComment = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.RichTextBox();
            this.rb1 = new System.Windows.Forms.RadioButton();
            this.rb2 = new System.Windows.Forms.RadioButton();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.rb4 = new System.Windows.Forms.RadioButton();
            this.rb5 = new System.Windows.Forms.RadioButton();
            this.rb7 = new System.Windows.Forms.RadioButton();
            this.rb8 = new System.Windows.Forms.RadioButton();
            this.rb9 = new System.Windows.Forms.RadioButton();
            this.rb10 = new System.Windows.Forms.RadioButton();
            this.rb6 = new System.Windows.Forms.RadioButton();
            this.pnlExit = new System.Windows.Forms.Panel();
            this.btn4Exit = new System.Windows.Forms.Button();
            this.lblATW = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.itemThumbnail)).BeginInit();
            this.pnlExit.SuspendLayout();
            this.SuspendLayout();
            // 
            // itemThumbnail
            // 
            this.itemThumbnail.Location = new System.Drawing.Point(16, 60);
            this.itemThumbnail.Name = "itemThumbnail";
            this.itemThumbnail.Size = new System.Drawing.Size(129, 167);
            this.itemThumbnail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemThumbnail.TabIndex = 0;
            this.itemThumbnail.TabStop = false;
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRate.Location = new System.Drawing.Point(161, 60);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(122, 20);
            this.lblRate.TabIndex = 2;
            this.lblRate.Text = "Rate this movie:";
            this.lblRate.Click += new System.EventHandler(this.lblRate_Click);
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComment.Location = new System.Drawing.Point(161, 116);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(146, 20);
            this.lblComment.TabIndex = 3;
            this.lblComment.Text = "Add your comment:";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(165, 139);
            this.txtComment.MaxLength = 120;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(370, 52);
            this.txtComment.TabIndex = 4;
            this.txtComment.Text = "";
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Location = new System.Drawing.Point(165, 83);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(31, 17);
            this.rb1.TabIndex = 6;
            this.rb1.TabStop = true;
            this.rb1.Text = "1";
            this.rb1.UseVisualStyleBackColor = true;
            this.rb1.CheckedChanged += new System.EventHandler(this.rb1_CheckedChanged);
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.Location = new System.Drawing.Point(202, 83);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(31, 17);
            this.rb2.TabIndex = 7;
            this.rb2.TabStop = true;
            this.rb2.Text = "2";
            this.rb2.UseVisualStyleBackColor = true;
            this.rb2.CheckedChanged += new System.EventHandler(this.rb2_CheckedChanged);
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Location = new System.Drawing.Point(239, 83);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(31, 17);
            this.rb3.TabIndex = 8;
            this.rb3.TabStop = true;
            this.rb3.Text = "3";
            this.rb3.UseVisualStyleBackColor = true;
            this.rb3.CheckedChanged += new System.EventHandler(this.rb3_CheckedChanged);
            // 
            // rb4
            // 
            this.rb4.AutoSize = true;
            this.rb4.Location = new System.Drawing.Point(276, 83);
            this.rb4.Name = "rb4";
            this.rb4.Size = new System.Drawing.Size(31, 17);
            this.rb4.TabIndex = 9;
            this.rb4.TabStop = true;
            this.rb4.Text = "4";
            this.rb4.UseVisualStyleBackColor = true;
            this.rb4.CheckedChanged += new System.EventHandler(this.rb4_CheckedChanged);
            // 
            // rb5
            // 
            this.rb5.AutoSize = true;
            this.rb5.Location = new System.Drawing.Point(313, 83);
            this.rb5.Name = "rb5";
            this.rb5.Size = new System.Drawing.Size(31, 17);
            this.rb5.TabIndex = 10;
            this.rb5.TabStop = true;
            this.rb5.Text = "5";
            this.rb5.UseVisualStyleBackColor = true;
            this.rb5.CheckedChanged += new System.EventHandler(this.rb5_CheckedChanged);
            // 
            // rb7
            // 
            this.rb7.AutoSize = true;
            this.rb7.Location = new System.Drawing.Point(387, 82);
            this.rb7.Name = "rb7";
            this.rb7.Size = new System.Drawing.Size(31, 17);
            this.rb7.TabIndex = 12;
            this.rb7.TabStop = true;
            this.rb7.Text = "7";
            this.rb7.UseVisualStyleBackColor = true;
            this.rb7.CheckedChanged += new System.EventHandler(this.rb7_CheckedChanged);
            // 
            // rb8
            // 
            this.rb8.AutoSize = true;
            this.rb8.Location = new System.Drawing.Point(424, 83);
            this.rb8.Name = "rb8";
            this.rb8.Size = new System.Drawing.Size(31, 17);
            this.rb8.TabIndex = 13;
            this.rb8.TabStop = true;
            this.rb8.Text = "8";
            this.rb8.UseVisualStyleBackColor = true;
            this.rb8.CheckedChanged += new System.EventHandler(this.rb8_CheckedChanged);
            // 
            // rb9
            // 
            this.rb9.AutoSize = true;
            this.rb9.Location = new System.Drawing.Point(461, 83);
            this.rb9.Name = "rb9";
            this.rb9.Size = new System.Drawing.Size(31, 17);
            this.rb9.TabIndex = 14;
            this.rb9.TabStop = true;
            this.rb9.Text = "9";
            this.rb9.UseVisualStyleBackColor = true;
            this.rb9.CheckedChanged += new System.EventHandler(this.rb9_CheckedChanged);
            // 
            // rb10
            // 
            this.rb10.AutoSize = true;
            this.rb10.Location = new System.Drawing.Point(498, 83);
            this.rb10.Name = "rb10";
            this.rb10.Size = new System.Drawing.Size(37, 17);
            this.rb10.TabIndex = 15;
            this.rb10.TabStop = true;
            this.rb10.Text = "10";
            this.rb10.UseVisualStyleBackColor = true;
            this.rb10.CheckedChanged += new System.EventHandler(this.rb10_CheckedChanged);
            // 
            // rb6
            // 
            this.rb6.AutoSize = true;
            this.rb6.Location = new System.Drawing.Point(350, 83);
            this.rb6.Name = "rb6";
            this.rb6.Size = new System.Drawing.Size(31, 17);
            this.rb6.TabIndex = 16;
            this.rb6.TabStop = true;
            this.rb6.Text = "6";
            this.rb6.UseVisualStyleBackColor = true;
            this.rb6.CheckedChanged += new System.EventHandler(this.rb6_CheckedChanged);
            // 
            // pnlExit
            // 
            this.pnlExit.BackColor = System.Drawing.Color.Transparent;
            this.pnlExit.Controls.Add(this.btn4Exit);
            this.pnlExit.Location = new System.Drawing.Point(551, 0);
            this.pnlExit.Name = "pnlExit";
            this.pnlExit.Size = new System.Drawing.Size(41, 32);
            this.pnlExit.TabIndex = 17;
            // 
            // btn4Exit
            // 
            this.btn4Exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn4Exit.FlatAppearance.BorderSize = 0;
            this.btn4Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn4Exit.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4Exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.btn4Exit.Location = new System.Drawing.Point(3, 0);
            this.btn4Exit.Name = "btn4Exit";
            this.btn4Exit.Size = new System.Drawing.Size(38, 32);
            this.btn4Exit.TabIndex = 0;
            this.btn4Exit.Text = "X";
            this.btn4Exit.UseVisualStyleBackColor = true;
            this.btn4Exit.Click += new System.EventHandler(this.btn4Exit_Click);
            // 
            // lblATW
            // 
            this.lblATW.AutoSize = true;
            this.lblATW.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATW.Location = new System.Drawing.Point(16, 24);
            this.lblATW.Name = "lblATW";
            this.lblATW.Size = new System.Drawing.Size(129, 20);
            this.lblATW.TabIndex = 18;
            this.lblATW.Text = "Add to Watchlist:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Georgia", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(161, 21);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(56, 23);
            this.lblTitle.TabIndex = 19;
            this.lblTitle.Text = "Title";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSave.Location = new System.Drawing.Point(165, 197);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 30);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // WatchlistItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(592, 254);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblATW);
            this.Controls.Add(this.pnlExit);
            this.Controls.Add(this.rb6);
            this.Controls.Add(this.rb10);
            this.Controls.Add(this.rb9);
            this.Controls.Add(this.rb8);
            this.Controls.Add(this.rb7);
            this.Controls.Add(this.rb5);
            this.Controls.Add(this.rb4);
            this.Controls.Add(this.rb3);
            this.Controls.Add(this.rb2);
            this.Controls.Add(this.rb1);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.lblRate);
            this.Controls.Add(this.itemThumbnail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WatchlistItemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "y";
            this.Load += new System.EventHandler(this.WatchlistItemForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.itemThumbnail)).EndInit();
            this.pnlExit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox itemThumbnail;
        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.RichTextBox txtComment;
        private System.Windows.Forms.RadioButton rb1;
        private System.Windows.Forms.RadioButton rb2;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.RadioButton rb4;
        private System.Windows.Forms.RadioButton rb5;
        private System.Windows.Forms.RadioButton rb7;
        private System.Windows.Forms.RadioButton rb8;
        private System.Windows.Forms.RadioButton rb9;
        private System.Windows.Forms.RadioButton rb10;
        private System.Windows.Forms.RadioButton rb6;
        private System.Windows.Forms.Panel pnlExit;
        private System.Windows.Forms.Button btn4Exit;
        private System.Windows.Forms.Label lblATW;
        public System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnSave;
    }
}