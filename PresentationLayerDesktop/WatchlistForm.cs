﻿using Business_Layer;
using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerDesktop
{
    public partial class WatchlistForm : Form
    {
        public Point mouseLocation;

        WatchlistBusiness watchlistBusiness = new WatchlistBusiness();
        MovieBusiness movieBusiness = new MovieBusiness();
        UserBusiness userBusiness = new UserBusiness();
        public WatchlistForm()
        {
            InitializeComponent();
        }

        public void WatchlistForm_Load(object sender, EventArgs e)
        {
            listWatchlist();
            fillSearch();

            StreamReader sr = new StreamReader("LoggedUser.txt");
            int userID = Int32.Parse(sr.ReadLine());
            sr.Close();
            User user = userBusiness.GetUserById(userID);
            lblLoggedUser.Text = "Welcome, " + user.Username; 
        }

        public void listWatchlist()
        {
            StreamReader sr = new StreamReader("LoggedUser.txt");
            int userID = Int32.Parse(sr.ReadLine());
            sr.Close();

            List<Watchlist> lista = watchlistBusiness.GetWatchlist(userID);
            ListItem[] listItem = new ListItem[lista.Count];
            Movie[] movieList = new Movie[lista.Count];

            for (int i = 0; i < lista.Count; i++)
            {
                movieList[i] = movieBusiness.GetMovie(lista[i].MovieID);
            }

            flowLayoutPanel.Controls.Clear();
            for (int i = 0; i < lista.Count; i++)
            {
                listItem[i] = new ListItem();
                listItem[i].thumbnail.ImageLocation = $"{movieList[i].Thumbnail}";
                listItem[i].lblTitle.Text = movieList[i].Title;
                listItem[i].lblGenre.Text = movieList[i].Genre;
                listItem[i].lblDescription.Text = movieList[i].Description;
                listItem[i].lblStars.Text = movieList[i].Actors;
                listItem[i].lblComment.Text = lista[i].Comment;
                listItem[i].lblRate.Text = "☆" + lista[i].Rate.ToString();

                flowLayoutPanel.Controls.Add(listItem[i]);
            }
            if(!lista.Any())
            {
                lblMessage.Text = "No Movies in Watchlist";
            }
            else
            {
                lblMessage.Text = "";
            }
        }

        private void btn3Exit_Click(object sender, EventArgs e)
        {
            this.Close();
            File.Delete(@"LoggedUser.txt");
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginForm logForm = new LoginForm();
            logForm.Show();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();

            string valueToSearch = textBoxSearch.Text;

            List<Movie> movieList = movieBusiness.SearchMovies(valueToSearch);
            MovieItem[] listItem = new MovieItem[movieList.Count];

            for (int i = 0; i < movieList.Count; i++)
            {
                listItem[i] = new MovieItem();
                listItem[i].thumbnail.ImageLocation = $"{movieList[i].Thumbnail}";
                listItem[i].lblTitle.Text = movieList[i].Title;
                listItem[i].lblGenre.Text = movieList[i].Genre;
                listItem[i].lblDescription.Text = movieList[i].Description;
                listItem[i].lblStars.Text = movieList[i].Actors;

                flowLayoutPanel2.Controls.Add(listItem[i]);
            }
        }
        private void fillSearch()
        {
            List<Movie> movieList = movieBusiness.GetAllMovies();
            MovieItem[] listItem = new MovieItem[movieList.Count];

            for (int i = 0; i < movieList.Count; i++)
            {
                listItem[i] = new MovieItem();
                listItem[i].thumbnail.ImageLocation = $"{movieList[i].Thumbnail}";
                listItem[i].lblTitle.Text = movieList[i].Title;
                listItem[i].lblGenre.Text = movieList[i].Genre;
                listItem[i].lblDescription.Text = movieList[i].Description;
                listItem[i].lblStars.Text = movieList[i].Actors;

                flowLayoutPanel2.Controls.Add(listItem[i]);
            }

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseLocation = new Point(-e.X, -e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePose = Control.MousePosition;
                mousePose.Offset(mouseLocation.X, mouseLocation.Y);
                Location = mousePose;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            listWatchlist();
        }

        private void btnLogout2_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginForm logForm = new LoginForm();
            logForm.Show();
        }
    }
}
