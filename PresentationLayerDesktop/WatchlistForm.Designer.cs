﻿namespace PresentationLayerDesktop
{
    partial class WatchlistForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WatchlistForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLoggedUser = new System.Windows.Forms.Label();
            this.btn3Exit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.wTabs = new MetroFramework.Controls.MetroTabControl();
            this.tabSearch = new MetroFramework.Controls.MetroTabPage();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnLogout2 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblSearchMovie = new System.Windows.Forms.Label();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.tabWatchlist = new MetroFramework.Controls.MetroTabPage();
            this.lblMessage = new System.Windows.Forms.Label();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.wTabs.SuspendLayout();
            this.tabSearch.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabWatchlist.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.panel1.Controls.Add(this.lblLoggedUser);
            this.panel1.Controls.Add(this.btn3Exit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 32);
            this.panel1.TabIndex = 14;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // lblLoggedUser
            // 
            this.lblLoggedUser.AutoSize = true;
            this.lblLoggedUser.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblLoggedUser.Location = new System.Drawing.Point(12, 9);
            this.lblLoggedUser.Name = "lblLoggedUser";
            this.lblLoggedUser.Size = new System.Drawing.Size(35, 13);
            this.lblLoggedUser.TabIndex = 15;
            this.lblLoggedUser.Text = "label1";
            // 
            // btn3Exit
            // 
            this.btn3Exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn3Exit.FlatAppearance.BorderSize = 0;
            this.btn3Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn3Exit.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3Exit.ForeColor = System.Drawing.Color.White;
            this.btn3Exit.Location = new System.Drawing.Point(804, 0);
            this.btn3Exit.Name = "btn3Exit";
            this.btn3Exit.Size = new System.Drawing.Size(38, 32);
            this.btn3Exit.TabIndex = 0;
            this.btn3Exit.Text = "X";
            this.btn3Exit.UseVisualStyleBackColor = true;
            this.btn3Exit.Click += new System.EventHandler(this.btn3Exit_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.wTabs);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(842, 473);
            this.panel2.TabIndex = 15;
            // 
            // wTabs
            // 
            this.wTabs.Controls.Add(this.tabSearch);
            this.wTabs.Controls.Add(this.tabWatchlist);
            this.wTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wTabs.Location = new System.Drawing.Point(0, 0);
            this.wTabs.Name = "wTabs";
            this.wTabs.SelectedIndex = 0;
            this.wTabs.Size = new System.Drawing.Size(842, 473);
            this.wTabs.Style = MetroFramework.MetroColorStyle.Red;
            this.wTabs.TabIndex = 0;
            this.wTabs.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.wTabs.UseSelectable = true;
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.flowLayoutPanel2);
            this.tabSearch.Controls.Add(this.btnLogout2);
            this.tabSearch.Controls.Add(this.panel3);
            this.tabSearch.HorizontalScrollbarBarColor = true;
            this.tabSearch.HorizontalScrollbarHighlightOnWheel = false;
            this.tabSearch.HorizontalScrollbarSize = 10;
            this.tabSearch.Location = new System.Drawing.Point(4, 38);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Size = new System.Drawing.Size(834, 431);
            this.tabSearch.TabIndex = 0;
            this.tabSearch.Text = "Search";
            this.tabSearch.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tabSearch.VerticalScrollbarBarColor = true;
            this.tabSearch.VerticalScrollbarHighlightOnWheel = false;
            this.tabSearch.VerticalScrollbarSize = 10;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoScroll = true;
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(8, 38);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(815, 361);
            this.flowLayoutPanel2.TabIndex = 18;
            // 
            // btnLogout2
            // 
            this.btnLogout2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.btnLogout2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLogout2.ForeColor = System.Drawing.Color.White;
            this.btnLogout2.Location = new System.Drawing.Point(751, 405);
            this.btnLogout2.Name = "btnLogout2";
            this.btnLogout2.Size = new System.Drawing.Size(75, 23);
            this.btnLogout2.TabIndex = 17;
            this.btnLogout2.Text = "Logout";
            this.btnLogout2.UseVisualStyleBackColor = false;
            this.btnLogout2.Click += new System.EventHandler(this.btnLogout2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.panel3.Controls.Add(this.lblSearchMovie);
            this.panel3.Controls.Add(this.buttonSearch);
            this.panel3.Controls.Add(this.textBoxSearch);
            this.panel3.Location = new System.Drawing.Point(209, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(334, 32);
            this.panel3.TabIndex = 6;
            // 
            // lblSearchMovie
            // 
            this.lblSearchMovie.AutoSize = true;
            this.lblSearchMovie.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchMovie.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.lblSearchMovie.Location = new System.Drawing.Point(14, 6);
            this.lblSearchMovie.Name = "lblSearchMovie";
            this.lblSearchMovie.Size = new System.Drawing.Size(104, 18);
            this.lblSearchMovie.TabIndex = 4;
            this.lblSearchMovie.Text = "Search Movie";
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.buttonSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSearch.BackgroundImage")));
            this.buttonSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSearch.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonSearch.Location = new System.Drawing.Point(289, 6);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(25, 21);
            this.buttonSearch.TabIndex = 3;
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(124, 6);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(159, 20);
            this.textBoxSearch.TabIndex = 2;
            // 
            // tabWatchlist
            // 
            this.tabWatchlist.BackColor = System.Drawing.Color.Salmon;
            this.tabWatchlist.Controls.Add(this.lblMessage);
            this.tabWatchlist.Controls.Add(this.flowLayoutPanel);
            this.tabWatchlist.Controls.Add(this.btnRefresh);
            this.tabWatchlist.Controls.Add(this.btnLogout);
            this.tabWatchlist.HorizontalScrollbarBarColor = true;
            this.tabWatchlist.HorizontalScrollbarHighlightOnWheel = false;
            this.tabWatchlist.HorizontalScrollbarSize = 10;
            this.tabWatchlist.Location = new System.Drawing.Point(4, 38);
            this.tabWatchlist.Name = "tabWatchlist";
            this.tabWatchlist.Size = new System.Drawing.Size(834, 431);
            this.tabWatchlist.Style = MetroFramework.MetroColorStyle.Blue;
            this.tabWatchlist.TabIndex = 1;
            this.tabWatchlist.Text = "Watchlist";
            this.tabWatchlist.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tabWatchlist.VerticalScrollbarBarColor = true;
            this.tabWatchlist.VerticalScrollbarHighlightOnWheel = false;
            this.tabWatchlist.VerticalScrollbarSize = 10;
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblMessage.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.SystemColors.Control;
            this.lblMessage.Location = new System.Drawing.Point(8, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(177, 11);
            this.lblMessage.TabIndex = 1;
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.AutoScroll = true;
            this.flowLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.flowLayoutPanel.Location = new System.Drawing.Point(11, 14);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(815, 388);
            this.flowLayoutPanel.TabIndex = 2;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.Control;
            this.btnRefresh.Location = new System.Drawing.Point(670, 408);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 17;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(27)))), ((int)(((byte)(35)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLogout.ForeColor = System.Drawing.SystemColors.Control;
            this.btnLogout.Location = new System.Drawing.Point(751, 408);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(75, 23);
            this.btnLogout.TabIndex = 16;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // WatchlistForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 505);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WatchlistForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WatchlistForm";
            this.Load += new System.EventHandler(this.WatchlistForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.wTabs.ResumeLayout(false);
            this.tabSearch.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabWatchlist.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn3Exit;
        private System.Windows.Forms.Label lblLoggedUser;
        private System.Windows.Forms.Panel panel2;
        private MetroFramework.Controls.MetroTabControl wTabs;
        private MetroFramework.Controls.MetroTabPage tabSearch;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.TextBox textBoxSearch;
        private MetroFramework.Controls.MetroTabPage tabWatchlist;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnRefresh;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.Button btnLogout2;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label lblSearchMovie;
        private System.Windows.Forms.Label lblMessage;
    }
}