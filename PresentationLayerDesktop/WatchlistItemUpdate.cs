﻿using Business_Layer;
using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerDesktop
{
    public partial class WatchlistItemUpdate : Form
    {
        WatchlistBusiness watchlistBusiness = new WatchlistBusiness();
        MovieBusiness movieBusiness = new MovieBusiness();
        UserBusiness userBusiness = new UserBusiness();
        public WatchlistItemUpdate()
        {
            InitializeComponent();
        }

        private void lblRate_Click(object sender, EventArgs e)
        {

        }

        private void WatchlistItemUpdate_Load(object sender, EventArgs e)
        {
            getMovie();
        }

        private void getMovie()
        {
            StreamReader sr = new StreamReader("MovieUpdate.txt");
            int movieItemId = Int32.Parse(sr.ReadLine());
            sr.Close();
            Movie movie = movieBusiness.GetMovie(movieItemId);

            StreamReader srt = new StreamReader("LoggedUser.txt");
            int userID = Int32.Parse(srt.ReadLine());
            srt.Close();

            Watchlist watchlist = watchlistBusiness.GetWatchlistByUserMovie(userID, movieItemId);

            int rate = watchlist.Rate;

            switch(rate)
            {
                case 1:
                    rb1.Checked = true;
                    break;
                case 2:
                    rb2.Checked = true;
                    break;
                case 3:
                    rb3.Checked = true;
                    break;
                case 4:
                    rb4.Checked = true;
                    break;
                case 5:
                    rb5.Checked = true;
                    break;
                case 6:
                    rb6.Checked = true;
                    break;
                case 7:
                    rb7.Checked = true;
                    break;
                case 8:
                    rb8.Checked = true;
                    break;
                case 9:
                    rb9.Checked = true;
                    break;
                case 10:
                    rb10.Checked = true;
                    break;
            }
            

            txtComment.Text = watchlist.Comment;

            lblTitle.Text = movie.Title;
            itemThumbnail.ImageLocation = movie.Thumbnail;


        }

        int rateValue;
        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 1;
        }

        private void rb2_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 2;
        }

        private void rb3_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 3;
        }

        private void rb4_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 4;
        }

        private void rb5_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 5;
        }

        private void rb6_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 6;
        }

        private void rb7_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 7;
        }

        private void rb8_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 8;
        }

        private void rb9_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 9;
        }

        private void rb10_CheckedChanged(object sender, EventArgs e)
        {
            rateValue = 10;
        }

        private void btn4Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            StreamReader srM = new StreamReader("MovieUpdate.txt");
            int movieUpdateId = Int32.Parse(srM.ReadLine());
            srM.Close();

            StreamReader sr = new StreamReader("LoggedUser.txt");
            int userID = Int32.Parse(sr.ReadLine());
            sr.Close();

            int Rate = rateValue;
            string Comment = txtComment.Text;
            watchlistBusiness.UpdateWatchlist(Rate, Comment, movieUpdateId, userID);

            MessageBox.Show("Succesfully updated!", "Watchlist updated!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //WatchlistForm wlf = new WatchlistForm();
            //wlf.listWatchlist();

            this.Close();
        }
    }
}
