﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Watchlist
    {
        public int MovieID { get; set; }
        public int UsersID { get; set; }
        public int Rate { get; set; }
        public string Comment { get; set; }
    }
}
