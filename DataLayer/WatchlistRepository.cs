﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class WatchlistRepository
    {
        string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ftnhubdb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public List<Watchlist> GetAllWatchlists()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM dbo.Watchlist";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                List<Watchlist> listToReturn = new List<Watchlist>();

                while (sqlDataReader.Read())
                {
                    Watchlist w = new Watchlist();
                    w.MovieID = sqlDataReader.GetInt32(0);
                    w.UsersID = sqlDataReader.GetInt32(1);
                    w.Rate = sqlDataReader.GetInt32(2);
                    w.Comment = sqlDataReader.GetString(3);


                    listToReturn.Add(w);
                }

                return listToReturn;
            }
        }
        public int InsertWatchlist(Watchlist w)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = $"INSERT INTO dbo.Watchlist(MovieID,UsersID,Rate,Comment) VALUES('{w.MovieID}', '{w.UsersID}', '{w.Rate}', '{w.Comment}')";

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int DeleteMovieFromWatchlist(int movieID, int userID)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = $"DELETE FROM dbo.Watchlist WHERE MovieID = '{movieID}' AND UsersID = '{userID}'";

                return sqlCommand.ExecuteNonQuery();
            }
        }
        public int UpdateWatchlist(int Rate, string Comment, int MovieID, int UserID)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                sqlCommand.CommandText = $"UPDATE dbo.Watchlist SET Rate = '{Rate}', Comment = '{Comment}' WHERE MovieID = '{MovieID}' AND UsersID = '{UserID}'";

                return sqlCommand.ExecuteNonQuery();
            }
        }

    }
}
