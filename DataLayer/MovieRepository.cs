﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class MovieRepository
    {
        string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ftnhubdb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public List<Movie> GetAllMovies()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM dbo.Movie";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                List<Movie> listToReturn = new List<Movie>();

                while (sqlDataReader.Read())
                {
                    Movie m = new Movie();
                    m.Id = sqlDataReader.GetInt32(0);
                    m.Title = sqlDataReader.GetString(1);
                    m.Genre = sqlDataReader.GetString(2);
                    m.Description = sqlDataReader.GetString(3);
                    m.Thumbnail = sqlDataReader.GetString(4);
                    m.Actors = sqlDataReader.GetString(5);

                    listToReturn.Add(m);
                }

                return listToReturn;
            }
        }
        public List<Movie> SearchMovies(string valueToSearch)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM dbo.Movie WHERE Title LIKE '%" + valueToSearch + "%' OR Genre LIKE '%" + valueToSearch + "%'" ;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                List<Movie> listToReturn = new List<Movie>();

                while (sqlDataReader.Read())
                {
                    Movie m = new Movie();
                    m.Id = sqlDataReader.GetInt32(0);
                    m.Title = sqlDataReader.GetString(1);
                    m.Genre = sqlDataReader.GetString(2);
                    m.Description = sqlDataReader.GetString(3);
                    m.Thumbnail = sqlDataReader.GetString(4);
                    m.Actors = sqlDataReader.GetString(5);

                    listToReturn.Add(m);
                }

                return listToReturn;
            }
        }
    }
}
