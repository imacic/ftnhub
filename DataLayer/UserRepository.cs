﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class UserRepository
    {
        string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ftnhubdb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
    
         public List<User> GetAllUsers()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM dbo.Users";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                List<User> listToReturn = new List<User>();

                while (sqlDataReader.Read())
                {
                    User u = new User();
                    u.Id = sqlDataReader.GetInt32(0);
                    u.Username = sqlDataReader.GetString(1);
                    u.Password = sqlDataReader.GetString(2);

                    listToReturn.Add(u);
                }

                return listToReturn;
            }
        }
        public int InsertUser(User u)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                //sqlCommand.CommandText = "INSERT INTO dbo.Users VALUES(" + string.Format(
                //   "'{0}','{1}',{2},'{3}'", u.Id, u.Username, u.Password, u.Confirm) + ")";

                sqlCommand.CommandText = $"INSERT INTO dbo.Users(Username,Password) VALUES('{u.Username}', '{u.Password}')";

                return sqlCommand.ExecuteNonQuery();
            }
        }



    }
}
