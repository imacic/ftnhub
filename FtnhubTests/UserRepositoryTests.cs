﻿using System;
using System.Collections.Generic;
using DataLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FtnhubTests
{
    [TestClass]
    public class UserRepositoryTests
    {
        public User user = new User();
        public UserRepository userRepository = new UserRepository();

        [TestInitialize]
        public void InitTestInsertUser()
        {
            user.Username = "testusername";
            user.Password = "testpassword";

            userRepository.InsertUser(user);

        }
        [TestMethod]
        public void InsertUserTest()
        {
            int counter = 0;

            List<User> lista = new List<User>();
            lista = userRepository.GetAllUsers();

            foreach (User u in lista)
            {
                if (u.Username == user.Username)
                {
                    counter++;
                }

            }
            Assert.AreNotEqual(0, counter);
        }
    }
}
