﻿using System;
using System.Collections.Generic;
using DataLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FtnhubTests
{
    [TestClass]
    public class WatchlistRepositoryTests
    {
        public Watchlist watchlist = new Watchlist();
        public WatchlistRepository watchlistRepository = new WatchlistRepository();

        [TestInitialize]
        public void InitTestInsertWatchlist()
        {
            watchlist.MovieID = 4;
            watchlist.UsersID = 1002;
            watchlist.Rate = 10;
            watchlist.Comment = "TestComment";

            watchlistRepository.InsertWatchlist(watchlist);
        }
        [TestMethod]
        public void InsertWatchlistTest()
        {
            int counter = 0;

            List<Watchlist> lista = new List<Watchlist>();
            lista = watchlistRepository.GetAllWatchlists();

            foreach (Watchlist w in lista)
            {
                if (w.MovieID == watchlist.MovieID && w.UsersID == watchlist.UsersID)
                {
                    counter++;
                }

            }
            Assert.AreNotEqual(0, counter);
        }
        [TestMethod]
        public void UpdateWatchlistTest()
        {
            int counter = 0;
            watchlist.Rate = 9;
            watchlist.Comment = "New test comment";
            watchlistRepository.UpdateWatchlist(watchlist.Rate,watchlist.Comment,watchlist.MovieID,watchlist.UsersID);
            var allWatchlists = watchlistRepository.GetAllWatchlists();
            foreach (Watchlist w in allWatchlists)
            {
                if (w.Rate == watchlist.Rate && w.Comment.Equals(watchlist.Comment))
                {
                    counter++;
                }
            }

            Assert.AreNotEqual(0, counter);
        }

        [TestCleanup]
        public void Cleanup()
        {
            watchlistRepository.DeleteMovieFromWatchlist(4, 1002);
        }
    }
}
